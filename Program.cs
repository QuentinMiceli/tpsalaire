﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Program
    {
        static void Main(string[] args)
        {
            Ouvrier o1 = new Ouvrier("ou01", "Miceli", "Quentin", 1996, 2016);
            Cadre c1 = new Cadre("cd01", "Dupont", "Jean-Pierre", 1956, 3);
            Patron p1 = new Patron("pa01", "Goldberg", "David", 1945, 12);

            Console.WriteLine(o1.ToString());
            Console.WriteLine(c1.ToString());
            Console.WriteLine(p1.ToString());
        }
    }
}
