﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Ouvrier : Employe
    {
        public const int smig = 2500;
        private int dateEntree;

        public int DateEntree
        {
            get { return dateEntree; }
            set { dateEntree = value; }
        }

        public Ouvrier(string matricule, string nom, string prenom, int dateNaissance, int dateEntree) : base(matricule, nom, prenom, dateNaissance)
        {
            DateEntree = dateEntree;
        }

        public override int GetSalaire()
        {
            int salaire = smig + (DateTime.Now.Year - DateEntree) * 100;
            return salaire > smig * 2 ? smig * 2 : salaire; //ternaire
        }
        public override string ToString()
        {
            return String.Format("{0} - Date entrée : {1} - salaire : {2}", base.ToString(), DateEntree, GetSalaire());
        }
    }
}
