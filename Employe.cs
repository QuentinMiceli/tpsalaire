﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    abstract class Employe
    {
        private string matricule;
        private string nom;
        private string prenom;
        private int dateNaissance;

        public string Matricule
        {
            get { return matricule; }
            set { matricule = value; }
        }
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        public int DateNaissance
        {
            get { return dateNaissance; }
            set { dateNaissance = value; }
        }

        public Employe(string matricule, string nom, string prenom, int dateNaissance)
        {
            Matricule = matricule;
            Nom = nom;
            Prenom = prenom;
            DateNaissance = dateNaissance;
        }
        public abstract int GetSalaire();
        public override string ToString()
        {
            return String.Format("{0} {1} {2}, né en {3}", Matricule, Prenom, Nom, DateNaissance);
        }
    }
}
