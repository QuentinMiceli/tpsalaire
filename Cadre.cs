﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Cadre : Employe
    {
        private int indice;

        public int Indice
        {
            get { return indice; }
            set { indice = value; }
        }

        public Cadre(string matricule, string nom, string prenom, int dateNaissance, int indice) : base(matricule, nom, prenom, dateNaissance)
        {
            Indice = indice;
        }
        public override int GetSalaire()
        {
            switch (Indice)
            {
                case 1:
                    return 13000;
                case 2:
                    return 15000;
                case 3:
                    return 17000;
                case 4:
                    return 20000;
                default:
                    throw new Exception();
            }
        }
        public override string ToString()
        {
            return String.Format("{0} - Indice : {1} - salaire : {2}", base.ToString(), Indice, GetSalaire());
        }
    }
}
