﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TP1
{
    class Patron : Employe
    {
        private int pourcentage;
        private int ca = 50000;

        public int Pourcentage
        {
            get { return pourcentage; }
            set { pourcentage = value; }
        }
        public int Ca
        {
            get { return ca; }
            set { ca = value; }
        }

        public Patron(string matricule, string nom, string prenom, int dateNaissance, int pourcentage) : base(matricule, nom, prenom, dateNaissance)
        {
            Pourcentage = pourcentage;
            Ca = ca;
        }
        public override int GetSalaire()
        {
            return Ca * Pourcentage / 100;
        }
        public override string ToString()
        {
            return String.Format("{0} - CA : {1}  - Pourcentage : {2} - salaire : {3}", base.ToString(), Ca, Pourcentage, GetSalaire());
        }
    }
}
